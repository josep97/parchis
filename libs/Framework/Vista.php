<?php

class Vista {
    /**
     * Atributos de la clase Vista
     */
    private $smarty;
    
    /**
     * Constructor de la clase Vista
     */
    function __construct() {
        //Incluimos la clase Smarty
        require './libs/smarty/Smarty.class.php';

        //Creamos el objeto Smarty
        $this->smarty = new Smarty;

        //Asignamos los directorios
        $this->smarty->template_dir = './vistas/';
        $this->smarty->compile_dir = './vistas_c/';
        $this->smarty->config_dir = './configs/';
        $this->smarty->cache_dir = './cache/';
    }
    
    /**
     * Método que muestra la vista de un objeto
     */
    function render($nombreVista, $variables = []){
        //Asignamos las variables en Smarty
        for($i = 0; $i < sizeof(array_keys($variables)); $i++){
            $this->smarty->assign(array_keys($variables)[$i], $variables[array_keys($variables)[$i]]);
        }
        
        //Mostramos la vista
        $this->smarty->display($nombreVista.'.tpl');
    }
    
    /**
     * Método que asigna variables al template
     */
    function asignarVariables($variables = []){
        //Asignamos las variables en Smarty
        for($i = 0; $i < sizeof(array_keys($variables)); $i++){
            $this->smarty->assign(array_keys($variables)[$i], $variables[array_keys($variables)[$i]]);
        }
    }
    
}

