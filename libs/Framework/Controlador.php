<?php

class Controlador {
    /**
     * Constructor de la clase Controlador
     */
    function __construct() {
        //Creamos la vista del controlador
        $this->vista = new Vista();
        
        //Asignamos al template la variable dominio
        $this->vista->asignarVariables(['dominio' => $_SERVER['SERVER_NAME']]);
    }
    
}

