<?php

/**
 * Framework Josep Recio
 */

class Framework {    
    /**
     * Constructor de la clase Framework
     */
    function __construct() {
        //Iniciamos la session
        session_start();
        
        //Si no se ha creado el entrono lo creamos
        if(!isset($_SESSION["entorno"])){
            $_SESSION["entorno"] = [];
        }
        
        //Obtenemos el parámetro URL
        if(isset($_GET['url'])){
            $url = $_GET['url'];
        }else{
            $url = '';
        }
        
        //Convertimos la url a un array a partir del /
        $url = explode('/', rtrim($url, '/'));
        
        //Comprovamos si existe el objeto que nos pide el usuario
        $clase = './controladores/'.$url[0].'.php';
        
        if(file_exists($clase)){
            //Incluimos la clase
            require_once $clase;
            
            //Creamos el controlador
            $controlador = new $url[0]($this);
            
            //Comprovamos si se quiere ejecutar algún método del controlador
            if(isset($url[1])){
                if(method_exists($url[0], $url[1])){
                    //Comprovamos si se han pasado parámetros
                    if(isset($url[2])){
                        //Ejecutamos el método del controlador con los parámetros
                        $controlador -> {$url[1]}(array_splice($url, 2));
                    }else{
                        //Ejecutamos el método del controlador sin parámetros
                        $controlador -> {$url[1]}();
                    }
                }else{
                    //Mostramos un error de que no existe el método
                    echo 'El metodo del controlador no existe';
                }
            }
        }else{
            //La clase no existe
            echo 'El controlador no existe';
        }
    }

    /**
     * Método que guarda una variable en el entorno
     * 
     * @param type $clave
     * @param type $valor
     */
    function guardarVariable($clave, $valor){
        //Guardamos el valor en el entorno
        $_SESSION["entorno"][$clave] = $valor;
    }
    
    /**
     * Método que obtiene una variable del entorno
     * 
     * @param type $clave
     * @return type
     */
    function obtenerVariable($clave){
        //Devolvemos el valor del entorno
        return $_SESSION["entorno"][$clave];
    }
    
}

