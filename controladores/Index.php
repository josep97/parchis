<?php

class Index extends Controlador{ 
    /**
     * Atributos de la clase Index
     */
    private $app;
    
    /**
     * Constructor de la clase Index
     */
    function __construct($app) {
        //Creamos el objeto superior
        parent::__construct();
        
        //Incluïmos las clases
        require_once './modelos/Partida.class.php';
        
        //Vínculamos la app
        $this->app = $app;
    }

    /**
     * Método que muestra el formulario para crear una nueva partida
     */
    function nuevaPartida(){
        //Creamos el objeto partida
        $partidaObj = new Partida($this->vista);
        
        //Mostramos el header
        $this->vista->render('header');
    
        //Mostramos el formulario para crear una nueva partida
        $partidaObj->mostrarFormularioNuevaPartida();
        
        //Mostramos el footer
        $this->vista->render('footer');
    }
    
    /**
     * Método que crea una partida
     */
    function crearPartida(){
        
        
    }
    
}