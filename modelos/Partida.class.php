<?php

class Partida {
    /**
     * Atributos de la clase Partida
     */
    private $tablero;
    private $turno;
    private $totalJugadores;
    private $vista;
    
    /**
     * Constructor sin parámetros de la clase Partida
     */
    function __construct($vista, $totalJugadores = 0) {
        //Obtenemos el objeto vista para la clase Partida
        $this->vista = $vista;
        
        //Comprovamos si tenemos jugadores en la partida
        if($totalJugadores > 0){
            //Creamos el entorno de la partida
            $this->totalJugadores=$totalJugadores;
            $this->turno=0;
            $this->tablero=array(68);
            
            //Recorremos los jugadores que van a jugar
            for($i = 1; $i <= $totalJugadores; $i++){
                
            }
            
        }else{
            //Creamos un objeto vacio
            $this->tablero=[];
            $this->turno=0;
            $this->totalJugadores=0;
        }
    }

    /**
     * Método que muestra el formulario para crear una nueva partida
     */
    function mostrarFormularioNuevaPartida(){
        //Mostramos el formulario para iniciar una partida
        $this->vista->render('frm_nuevaPartida');
    }
    
}

